import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import { Navbar, NavItem, NavDropdown, MenuItem, Nav } from 'react-bootstrap';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom'
import { Home } from './components/Home';
import {Alunos} from './components/Alunos';

function App() {
  return (
    <Container className="Container">
      <BrowserRouter>
        <Navbar bg='light' expand='lg'>
          <Navbar.Brand as={Link} to="/">Escola AbcLinguas</Navbar.Brand>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='mr-auto'>
              <Nav.Link as={Link} to="/">Home</Nav.Link>
              <NavDropdown title='Cadastros' id='basic-nav-dropdown'>
                <NavDropdown.Item as={Link} to='/alunos'>Alunos</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/alunos" component={Alunos} />
        </Switch>
      </BrowserRouter>
    </Container>
  );
}

export default App;
