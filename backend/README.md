# AbcLinguas API

- Este projeto consiste em um exemplo de uma API Web REST usando ASP.NET Core, EF Core e Banco de Dados PostgreSQL. 

## [Tutoriais](https://gitlab.com/luizfernando.fepi/abclinguas-api/-/wikis/Home)

- A [Wiki](https://gitlab.com/luizfernando.fepi/abclinguas-api/-/wikis/Home) contém tutoriais para permitir que você consiga criar sua API Web com estas tecnologias.


