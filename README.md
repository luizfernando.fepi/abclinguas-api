# Sistema AbcLinguas

Este projeto consiste em um exemplo de uma aplicação Web desenvolvida durante a disciplina Mobile/Web Development:

### Backend 
Consiste em uma API Web REST usando ASP.NET Core, EF Core e Banco de Dados PostgreSQL. 

### Frontend
Consiste uma página Web criada com React (contém o CRUD para Alunos)

## [Tutoriais](https://gitlab.com/luizfernando.fepi/abclinguas-api/-/wikis/Home)

- A [Wiki](https://gitlab.com/luizfernando.fepi/abclinguas-api/-/wikis/Home) contém tutoriais para permitir que você consiga criar sua API Web com estas tecnologias.

